//console.log("Hello World!");

// [SECTION] - Parameters and Arguments
// Functions in JS are lines/blocks of codes that tells our device/application to perform a certain task.

//Last session (s17) we learned how to use a basic function

function printInput(){
	let nickname = prompt("Enter your nickname.");
	console.log("Hi, " + nickname);
}

/*printInput();*/

// In some cases a basic function may not be ideal
// for other cases, functions can also process data directly into it instead.

// Consider this function
// parameter is located inside the "(parameter)" after the function name.
function printName(name){
	console.log("My name is " + name);
}

// argument is located in the invocation of the function. the data then will be passed on the parameter which can be used inside the code blocks of a particular function.
printName("Juana");
printName("John");
printName("Jane");

// variables can also be passed as an argument
let sampleVariable = "Yui";

printName(sampleVariable);

// Function arguments cannot be used by a function if there are no parameters provided.

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " +remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?")
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// Functions as arguments

function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);
//finding more info about a function, we can use console.log([function_name])
console.log(argumentFunction);

// Function - Multiple Parameters
// Multiple "arguments" will correspond to the number of "parameters"

function createFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Juan","deLa", "Cruz")

// "juan" will be stored in the parameter "firstName"
// "deLa" will be stored in the parameter "middleName"
// "Cruz" will be stored in the parameter "lastName"

createFullName("Juan", "deLa") //returns undefined for lastName
createFullName("Juan","deLa", "Cruz", "hello") // returns until lastName only

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName,middleName,lastName);

// Parameters names are just names to refer to the argument. 
// The order of the argument has to be same with the parameters

function printFullName(middleName, firstName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

printFullName("Juan","deLa", "Cruz");

// The Return Statement

function returnFullName(firstName, middleName, lastName){
	console.log("test console message")
	return firstName + " " + middleName + " " + lastName;
	console.log("this message will not be printed")
}

let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);

/*returnFullName("Jeffrey", "Smith", "Bezos");*/
// this will not work if our functions is expecting a return value, returned values should be stored in a variable.

console.log(returnFullName(firstName, middleName, lastName));

//you can also create a variable inside the function to contain the result and return that variable instead.

function returnAddress(city, country){
	let fullAddress = city + ", " + country;
	return fullAddress;
}

let myAddress = returnAddress("Cebu City", "Philippines");
console.log(myAddress);

// on the other hand, when a function only has a console.log() to display its result undefined instead.

function printPlayerInfo(username, level, job){
	console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Job: " + job);
}

let user1 = printPlayerInfo("knight_white", 95, "Paladin");
console.log(user1);

//returns undefined because printPlayerInfo returns nothing.